\documentclass[11pt,a4paper,oneside]{article}
\linespread{1.2}   
  %pacchetti indispensabili
  \usepackage{amsmath,amscd,amsfonts,amssymb}
  \usepackage[english]{babel}
  \usepackage[utf8]{inputenc} %attenzione codifica UTF8
  \usepackage[a4paper,top=2.25cm,bottom=2.25cm,left=2cm,right=2cm]{geometry}
  \usepackage{graphicx}  
  \usepackage{float}
  \usepackage{hyperref}
  \usepackage{color}
  \usepackage{xcolor}
  \usepackage{tikz}
  \usepackage{enumitem}
  \usetikzlibrary{decorations.pathreplacing}
  \usepackage{booktabs} % commands \toprule for tables


%%to write date/time in the page
\usepackage{fancyhdr}
\usepackage{datetime}
\fancyhead[R]{\scriptsize{\today\ \currenttime}}
\pagestyle{fancy}


  
  \begin{document}

  
\section*{The complete problem formulation}
In this chapter, we extend the simplified model presented in the previous chapter in order to capture the full problem as described \textcolor{red}{in chapter 2}: we consider a set of different ships and a pool of different ranks seafarers. As a consequence of this, now we also deal with the constraint regarding the minimum experience for combinations of ranks.\\\\
As in the simplified case, every seafarer is supposed to perform a shift always on the same ship, and the objective of the model is the minimization of travel costs from ports to home-airports and vice-versa.\\
Even though many constraints in this model are similar to the ones of the model for the simplified problem, all of them will be described again.\\\\
The new model must include the following requirements:
\begin{enumerate}
\item The number of seafarers on board is defined for every rank and it must be exactly the same as the one required
\item Seafarers must stay on the same ship for the whole duration of an on-board period
\item On-board period must be included between the minimum-maximum range allowed, according to seafarer's rank
\item Minimum off-board period duration depends on the period spent on-board and a ratio specific for every rank. It can be violated by a constant.
\item When an exchange between two seafarers of the same rank happens on the same ship, a \textit{handover period }is necessary.
\item For some combinations of ranks, at least one of the seafarers on board belonging to these ranks must have a \textit{minimum required experience} (in the company or in the rank).\\
For example, let $r_1$ and $r_2$ be two ranks, $c_1$ and $c_2$ two seafarers whose ranks are respectively $r_1$ and $r_2$. Finally, let $l$ be the required minimum months experience in the rank (company) for ranks $r_1,r_2$, while $e_1,\,e_2$ are seafarers' respectively months experience in the rank (company). The constraint says that at least one of the following must be true: $ e_1\geq l\quad$or$\quad e_2 \geq l$.
\end{enumerate}

Given the similarity of this problem to the simplified one \textcolor{red}{ seen in chapter 3}, the model proposed in the following can be easily recognize as an extension of it: many of the constraints have a very similar structure to the ones in the simplified model, with the addition of some indexes related to the ship ($s$) or seafarer $c$'s rank ($r=r(c)$). \\
For example, every ship stops at different ports in different moments, and so it is necessary to define a set of indexes of times for every ship ($T^s$). Also variables which regulate every seafarer's going on/off board must contain an index for the ship. \\
Another example is the set of time indexes for the handover period. In fact, in the previous model we defined the set $H^t_c$, but now different ships have different times, so an index regarding ship $s$ is required. \\
%Moreover, handover period varies according to seafarers' rank.. \\
%These changes have a consequence on the formulation of constraints.\\ For example, if in the simplified model we indicated the end time of shift $i$ on-board period for seafarer $c$ as
%\begin{equation*}
%\sum_{t\in T} \sum_{k\in H^t_c} d(k) \cdot X^{OFF,i}_{c,t,k}
%\end{equation*}
%now we have to specify also the ship $s$ and the rank of seafarer $c$:
%\begin{equation*}
%\sum_{s\in S}\sum_{t\in T^s} \sum_{k\in H^t_{r(c),s}} d(k,s) \cdot X^{OFF,i}_{c,t,k,s}
%\end{equation*}
%where $d(k,s)$ refers to the time elapsed from the beginning of the time horizon and time $k\in T^s$ .\\
%


Finally, now we have to model the requirement of minimum experience for combinations of ranks: we have a set of rules, indexed by elements of a set $U$, and every rule $u$ regards a different combination of $m=m(u)$ ranks, $\bar{R}_u=\{r_1(u),...,r_m(u)\}$. Every rule $u$ states that there must be at least $n(u)$ seafarers of ranks $r\in \bar{R}_u$ with a minimum experience $l_{r,u}>0$ in every moment and on every ship.\\
The idea for modelling this constraint is to define a seafarer $c$ as \textit{experienced} in the rank/company according to rule $u$ ($\varepsilon_{c,u}=1$) if his experience ($e_c$) is at least equal to the one required by rule $u$ ($l_{r(c),u}$) and his rank is involved by rule $u$ ($ {l}_{r(c),u} >0 $). More precisely, we say that ${\varepsilon}_{c,u}=1 \Leftrightarrow {e}_c\geq  {l}_{r(c),u} >0 $.\\
 In this way, we can require that the number of experienced seafarers per rank/company on board is at least $n(u)$, for every rule $u$. \\ 


\subsection*{Definition of the model for the complete problem}

\subsubsection*{Sets}  
\begin{itemize}
\item[•] A = Airports, which represents seafarers' home bases
\item[•] C = Crewmembers (seafarers) 
\item[•] $H^t_{c,s}$ := $\{k\in T^s\} \; |\;\underline{h}_{r(c)} \leq   d(k,s)-d(t,s) \leq \bar{h}_{r(c)} \}$ = indexes of times available for the handover period,  $ \forall s\in S, t\in T^s, c\in C$ 
\item[•] $H^ {t_{end}}_{c,s}:=\{t_{end}\} \qquad \forall s\in S, c\in C$
\item[•]$H^{t_0}_{c,s}:=\{k\in T^s\; | \; \underline{h}_{r(c)}\leq d(k)+t_c^h\leq \bar{h}_{r(c)}\} \qquad \forall s\in S,\,c\in C,$ 
\item[•] $I_r$ = Shifts for a seafarer with rank $r$ $=\{1,2, ... I_r^{max}\} $ 
\item[•] P = Ports 
\item[•] R = Ranks
\item[•] S = Ships
\item[•] $T^s$ = set of indexes of time moments in which vessel $s$ visits some port =  $\{1,2,...,t_{max}^s=|T^s|\}$ 
%\item[•] $T=\bigcup_{s\in S} T^s=\{1,2,....,t_{end}\} \qquad \{t\}$
\item[•] U = $\{1,2,...,u_{max}  \}$ = set of indexes for the rules of combination of ranks for which a minimum experience is required

\end{itemize}

  
\subsubsection*{Parameters}
\begin{itemize}
\item[•] $a(c)$ := seafarer $c$'s home airport
\item[•] $d(t,s)$ = time elapsed between time 0 and time $t\in T^s$
\item[•] $e_c$ = months experience in the rank for seafarer $c$
\item[•] $\tilde{e}_c$ = months experience in the company for seafarer $c$
\item[•] $\varepsilon_{c,u}=1 \Leftrightarrow e_c\geq  l_{r(c),u} > 0$    (such as $c$ is enough experienced in the rank in respect of request $u$), 0 otherwise $\qquad \forall c\in C,\, u\in U$
\item[•] $\tilde{\varepsilon}_{c,u}=1 \Leftrightarrow \tilde{e}_c\geq  \tilde{l}_{r(c),u} >0 $  (such as $c$ is enough experienced in the company in respect of request $u$), 0 otherwise $\qquad \forall c\in C,\, u\in U$
\item[•] $f_{pa}$ = flight cost from port $p$ to an airport $a$ and vice-versa, $p\in P, a\in A$. \\ $f_{p(t_{end},s),a}=constant \qquad \forall s\in S,\, \forall a \in A$
\item[•] $\gamma_{c,s}$ = 1 if seafarer $c$ is working on ship $s$ at the beginning of the time horizon, 0 otherwise
\item[•] $\eta_{c,s}$ = 1 if seafarer $c$ is spending a handover on ship $s$ at the beginning of the time horizon
\item[•] $\underline{h}_r$ = minimum handover period, according to rank $r$
\item[•] $\bar{h}_r$ = maximum handover period, according to rank $r$
\item[•] $I^{max}_r$ = upper bound for the maximum number of shifts in a time horizon, according to rank.\\ $I^{max}_r=\left \lfloor{\frac{L}{(1+\pi_r)\cdot MinOn_r - v}}\right \rfloor + 2 $
\item[•] $l_{r,u}$ = minimum rank experience required to say that a seafarer of rank $r$ is experienced in the rank, according to rule $u$. 
\item[•] $\tilde{l}_{r,u}$ = minimum rank experience required to say that a seafarer of rank $r$ is experienced in the company, according to rule $u$ 
\item[•] $L$ = length of the time horizon
\item[•] $MinOn_r, \, MaxOn_r$ = Min/Max On days, according to rank $r$
\item[•] $n_{s,r}$ = number of seafarers of rank $r$ required on ship $s$
\item[•] $n(u)$ = number of experienced seafarers required by rule $u$
\item[•] $p(t,s)$ = port visited by vessel $s$ at time $t$ according to the schedule\\ $p:T^s\cup\{t_{end}\} \times S \rightarrow P$, where $p(t_{end},s)$ is a `dummy' port independent from $s$
\item[•] $\pi_r$ = Off/on board ratio, according to rank $r$
\item[•] $r(c)$ = rank of seafarer $c$
%\item[•] $\bar{R}(u)=\{r_1(u),...,r_m(u)\}$ = combination of $m(=m(u))$ ranks concerned by rule $u$
\item[•] $\rho_{c,r}$ = 1 if seafarer $c$'s rank is $r$, 0 otherwise
\item[•] $t^H_c>0 \Leftrightarrow$ seafarer $c$ is spending a holiday period at time 0. In this case the parameter refers to the length of the period that $c$ has been spending on holiday since his last off-board.
\item[•] $t^W_c$ = duration of the working period of the most recent shift of seafarer $c$ before the beginning of the time horizon (such as, if $c$ is on-board, then $t^W_c$ refers to the time that $c$ has been spending on board; if $c$ is on holiday, $t^W_c$ indicates the time that $c$ spent working before going off board ) 
\item[•] $t^h_c$ = duration of the handover period of the most recent shift of seafarer $c$ before the beginning of the time horizon
%At the very beginning of the scheduling, $\gamma_c, t^W_c$ and $t^H_c$ are set as 0.
\item[•] $t_0=0$: starting time of the time horizon, $d(t_0,s)=0 \leq d(1,s) \quad \forall s\in S$
\item[•] $t_{end}=1+\max \; \{t^s_{max}| s\in S\}    \in \mathbb{N}$, $d(t_{end},s)= L \geq d(t^s_{max},s)\; \forall s\in S$
\item[•] $v$ = maximum allowed violation (in days) between the on-board and the off-board periods
\end{itemize}


\subsubsection*{Variables}
\begin{itemize}
\item[•] $X^{ON,i}_{c,t,s}$ = 1 if seafarer $c$ starts his shift $i$ on ship $s$ at time $t$, 0 otherwise \\ $ \forall i\in I,\, c\in C, s\in S, t\in T^s \cup\{t_0\}$\\
Since time $t_0$ does not coincide with a port stop, we define $X^{ON,1}_{c,t_0,s}$ as a `dummy' variable: if seafarer $c$ is already on board at time $t_0$, then we assume that for the considered time horizon, he goes on board at time $t_0$. 
\item[•] $X^{OFF,i}_{c,t,k,s}$ = 1 if seafarer $c$ ends the on board activity of his shift $i$ on ship $s$  at time $t$ but stays on board until time $k$ (in order to respect the handover period constraint), 0 otherwise $\qquad \forall i\in I,\, c\in C,\, s\in S,\, t\in T^s \cup\{t_0,t_{end}\}, \, k\in H^t_{c,s}$  \\ Case $X^{OFF,1}_{c,t_0,k,s}$ refers to the fact that seafarer $c$ has to finish his handover period on ship $s$, and so he goes of board at a certain time $k\in H^{t_0}_{c,s}$
\item[•] $Y_{c,t,t+1,s}$ = 1 if $c$ is aboard ship $s$  between times $t$ and $t+1$, 0 otherwise \\ $\qquad \forall c\in C,\,s\in S,\, t\in \{t_0\}\cup T^s\setminus\{t^s_{max}\}$ 
\item[•] $Y_{c,t^s_{max},t_{end},s}$ = 1 if $c$ is on ship $s$  between times $t^s_{max}$ and $t_{end}$, 0 otherwise \\ $\qquad \forall c\in C,\,s\in S$  \\
\end{itemize}


Finally, in order to simplify the notation of some constraints, we define:
\begin{itemize}
\item[•] $d(t^{ON,i}_c):= \sum_{s\in S} \sum_{t\in T^s\cup\{t_0\}} d(t,s) \cdot X^{ON,i}_{c,t,s} \qquad \forall c\in C, i\in I$ (starting time of shift $i$ for seafarer $c$)\\
\item[•]  $d(t^{OFF,i}_c):= \sum_{s\in S} \sum_{t\in T^s\cup \{t_0,t_{end}\}} \sum_{k\in H^t_{c,s}} d(k,s) \cdot X^{OFF,1}_{c,t,k,s}  \qquad \forall c\in C, i\in I$ (ending time of working period of shift $i$ for seafarer $c$)
\end{itemize}
  
  \newpage
  
\subsubsection*{Constraints}
Constraints from \eqref{C_flow_node_0} to \eqref{C_off_othershifts} are an extension of the corresponding constraints in the model for the simplified problem.
 \begin{enumerate}


\item[•] ``Conservation of the flow''
%\begin{subequations}
\begin{align}
 Y_{c,0,1,s}-X^{ON,1}_{c,0,s}+\sum_{k\in H^0_{c,s}} X^{OFF,1}_{c,0,k}  &=0 \qquad \quad\forall c\in C,\, \forall s\in S  \label{C_flow_node_0}  \\
\sum_{s\in S} \sum_{i\in I} \left[ X^{ON,i}_{c,0,s} + \sum_{t\in T^s} \left( X^{ON,i}_{c,t,s} - \sum_{k\in H^t_{c,s}} X^{OFF,i}_{c,t,k,s} \right) -  X^{OFF,i}_{c,t_{end},t_{end},s} \right] &= 0  \hspace{24mm}\forall c\in C  \label{C_flow_nodes_c} \\
Y_{c,t,t+1,s} - Y_{c,t-1,t,s} + \sum_{i\in I} \left( \sum_{k\in H^t_{c,s}} X^{OFF,i}_{c,t,k,s} - X^{ON,i}_{c,t,s} \right) &=0 \qquad \forall c\in C,\,\forall s\in S, \, \forall t\in T^s  \label{C_flow_nodes_t}\\
Y_{c,t^s_{max},t_{end},s} - \sum_{i \in I} X^{OFF,i}_{c,t_{end},t_{end},s} &=0 \label{C_flow_node_t*} \qquad\quad \forall c\in C,\,\forall s\in S 
\end{align}
%\end{subequations}

As in the model for the simplified problem, we consider every seafarer and every time moment as a node in a graph, while variables represent the flow on every arc of the network (see fig. \textcolor{red}{\ref{flow_conservation}}  in the previous chapter). \\
In constraint \eqref{C_dummy Xon,1}, we force $X^{ON,1}_{c,0,s}=1$ if $\gamma_{c,s}=1$ or $\eta_{c,s}=1$ for some $s\in S$, and in the same way, if on-board period on ship $s$ for a seafarer $c$ does not end within $t^s_{max}$, we force variable $X^{OFF,i}_{c,t_{end},t_{end},s}$ to be 1 (constraint \eqref{C_off_must_follow_on}). Thanks to this two ``dummy'' time moments, we can say that for every node the out-going flow must be equal to the in-going one.  
\eqref{C_flow_node_0} refers to the flow of node $t_0=0$, while \eqref{C_flow_node_t*} to node $t=t_{end}$. For nodes $t\in T^s$, the in-coming flow is given by variables $Y_{c,t-1,t,s}$ and $ X^{ON,i}_{c,t,s}$ , while out-going one by $Y_{c,t,t+1,s}$ and $ X^{OFF,i}_{c,t,k,s}$ (see \eqref{C_flow_nodes_t}). Lastly, flow for nodes $c\in C$ is defined by \eqref{C_flow_nodes_c}.\\
%We can notice that constraint \eqref{C_flow_nodes_t} is the one which guarantees that a shift must be performed always on the same ship ($\forall s\in S, t\in T^s$, the outgoing flow must be equal to the incoming). % This guarantees that in \eqref{C_flow_nodes_c} it is possible to sum over S and it can not happen that $X^{ON,i}_{c,t_1,\mathbf{s_1}}=1=X^{OFF,i}_{c,t_2,k,\mathbf{s_2}}$ for some $c\in C,i\in I,t_1\in T^{s_1}, t_2 \in T^{s_2},k\in H^{t_2}_{r(c),s_2}$. 


\item[•] Number of crew members on board
\begin{subequations}
\begin{flalign}
\hspace{27mm}\sum_{c\in C} Y_{c,t,t+1,s} \cdot \rho_{r,c} & = n_{s,r} & \forall s\in S,\, t\in T^s\setminus\{t_{max}^s\}, r\in R 
\label{C_number_crewmembers_end_escluded} \\
\hspace{27mm}\sum_{c\in C} Y_{c,t_{max}^s,t_{end},s} \cdot \rho_{r,c} &  = n_{s,r} &\forall s\in S,\, r\in R 
\label{C_number_crewmembers_end}
\end{flalign}
\end{subequations}

On every ship $s$, in every period between two stops $t,t+1\in T^s$, the required number of seafarers per rank $r$ must be satisfied.



\item[•] Every shift has at most one on-board time and every `on-board' must be followed by an `off-board' (off-board can happen outside the time horizon, at time $t_{end}$)
\begin{flalign}
\hspace{30mm}\sum_{s\in S}\sum_{t\in T^s\cup\{0\}} X^{ON,i}_{c,t,s} & \leq 1 & \forall i\in I, c\in C  \label{C_at_most_one_on_off} \\
\hspace{30mm}\sum_{t\in T^s\cup\{0\}}  X^{ON,i}_{c,t,s} & =\sum_{t\in T^s\cup\{t_0,t_{end}\}} \sum_{k\in H^t_{c,s}} X^{OFF,i}_{c,t,k,s} &\forall c\in C, i\in I, s\in S 
\label{C_off_must_follow_on} 
\end{flalign}

Constraint \eqref{C_off_must_follow_on}, combined with \eqref{C_flow_nodes_t}, guarantees that a working period is performed on the same ship $s$.

\item[•] Shifts must be ordered 
\begin{flalign}
&\hspace{35mm}\sum_{s\in S}\sum_{t\in T^s}  X^{ON,i}_{c,t,s} \leq \sum_{s\in S}\sum_{t\in T^s\cup\{t_0\}}  \sum_{k\in H^t_{c,s}} X^{OFF,i-1}_{c,t,k,s} &  \forall c\in C, i\in I\setminus\{1\}
\label{C_shift_order}
\end{flalign}

\item[•] On-board and off-board condition for shift $i=1$ at time $t_0=$0
\begin{subequations}
\begin{flalign}
\hspace{50mm} X^{ON,1}_{c,0,s}&=\gamma_{c,s}+\eta_{c,s} \qquad &\forall c\in C, s\in S \label{C_dummy Xon,1}\\
\sum_{k\in H^0_{c,s}}X^{OFF,1}_{c,0,k,s}&=\eta_{c,s}  \qquad &\forall c\in C,\,s\in S \label{C_dummy Xoff,1}
\end{flalign}
\end{subequations}


If $\gamma_{c,s}=1$ or $\eta_{c,s}=1$ for some $s$, constraint \eqref{C_dummy Xon,1} states that the corresponding variables $X^{ON,1}_{c,0,s}$ must be equal to 1. As we have seen in the analogous constraint of the simplified model, this variable does not refer to the fact that seafarer $c$ goes on board at time 0, but it is a ``dummy" variable introduced in order to simplify the formulation of some constraints, such as the ones regarding conservation of the flow or the next \eqref{C_ON, shift1}.\\Moreover, if $\eta_{c,s}=1$ for some $s$, we define the `dummy' variable $X^{OFF,1}_{c,0,k,s},\; k\in  H^0_{c,s} $: this means that seafarer $c$ must finish his shift (such as complete the handover period) at a time $k$.

Constraints from \eqref{C_ON, shift1} to \eqref{C_off_othershifts} aim to guarantee the duration of on and off-board periods. For a better understanding of them, we recall the graphic visualization of different types of shifts in case ${I^{max}_r\leq 2 \; \forall r\in R }$ and $MaxOn_r\geq L$ for some $r$ .\\

For every $c\in C$ there are 8 cases. 
\\Colours of the bullet points in the constraints refer to segments of the same colour.
\begin{enumerate}[label=(\alph*)]
\item[•] $\sum_{s\in S} ( \gamma_{c,s}+\eta_{c,s}) = 1$: seafarer $c$ is on a ship (working or `handovering') at time 0.\\
In case $\eta_{c,s}=1$ for a ship $s$, such as seafarer $c$ finished working in the previous time horizon but he has not finish to spend his handover period, we suppose that $t_0$ and the time moment which is called $t'$ in the following figures coincide (see constraint \eqref{C_dummy Xoff,1}).

\item \label{C_off,on,off} $ $%\\
\begin{figure}[H]
\begin{tikzpicture}[xscale=5]
\draw[-][draw=green, ultra thick] (-.2,0) -- (.5,0);
\draw[-][draw=yellow, ultra thick] (.5,0) -- (1,0);
\draw[-][draw=orange, ultra thick] (1,0) -- (1.8,0);
\draw[-][draw=teal, ultra thick] (1.8,0) -- (2.3,0);

\node[above] at (-.2,0) {$t_0$};
\node[above] at (0.5,0.2) {$k'$};
\node[above] at (1,0.2) {$t''$};
\node[above] at (1.8,0.2) {$k'''$};
\node[above] at (2.3,0) {$t_{end}$};

\draw [thick] (2.3,-.1) -- (2.3,0.1);

\draw [thick] (-.2,-.1) node[below]{$X^{ON,1}_{c,t_0,s_1}$} -- (-.2,0.1);
\draw [thick] (0.5,-.1) node[below]{$X^{OFF,1}_{c,t',k',s_1}$} -- (0.5,0.1);
\draw [thick] (1,-.1) node[below]{$X^{ON,2}_{c,t'',s_2}$} -- (1,0.1);
\draw [thick] (1.8,-.1) node[below]{$X^{OFF,2}_{c,t''',k''',s_2}$} -- (1.8,0.1);

\node[above] at (0.25,0) {on };
\node[above] at (0.75,0) {off };
\node[above] at (1.4,0) {on};
\node[above] at (2,0) {off };
\end{tikzpicture}
\end{figure}

\item \label{C_off,on} $ $%\\
\begin{figure}[H]

\begin{tikzpicture}[xscale=5]
\draw[-][draw=green, ultra thick] (-.2,0) -- (.66,0);
\draw[-][draw=yellow, ultra thick] (.66,0) -- (1.33,0);
\draw[-][draw=orange, ultra thick] (1.33,0) -- (2.3,0);

\node[above] at (-.2,0) {$t_0$};
\node[above] at (0.66,0) {$k'$};
\node[above] at (1.33,0) {$t''$};
\node[above] at (2.3,0) {$t_{end}$};

\draw [thick] (-.2,-.1) node[below]{$X^{ON,1}_{c,t_0,s_1}$} -- (-.2,0.1);
\draw [thick] (0.66,-.1) node[below]{$X^{OFF,1}_{c,t',k',s_1}$} -- (0.66,0.1);
\draw [thick] (1.33,-.1) node[below]{$X^{ON,2}_{c,t'',s_2}$} -- (1.33,0.1);
\draw [thick] (2.3,-.1) node[below]{$X^{OFF,2}_{c,t_{end},t_{end},s_2}$} -- (2.3,0.1);
\node[above] at (0.33,0) {on };
\node[above] at (1,0) {off};
\node[above] at (1.66,0) {on};
\end{tikzpicture}
\end{figure}

\item \label{C_off} $ $%\\
\begin{figure}[H]

\begin{tikzpicture}[xscale=5]
\draw [thick] (-.2,-.1) node[below]{$X^{ON,1}_{c,t_0,s_1}$} -- (-.2,0.1);
\draw[-][draw=green, ultra thick] (-.2,0) -- (1,0);
\draw[-][draw=yellow, ultra thick] (1,0) -- (2.3,0);

\node[above] at (-.2,0) {$t_0$};
\node[above] at (1,0) {$k'$};
\node[above] at (2.3,0) {$t_{end}$};

\draw [thick] (1,-.1) node[below]{$X^{OFF,1}_{c,t',k',s_1}$} -- (1,0.1);
\draw [thick] (2.3,-.1)  -- (2.3,0.1);

\node[above] at (0.5,0) {on };
\node[above] at (1.5,0) {off };
\end{tikzpicture}
\end{figure}


\item \label{C_never_off} $ $%\\
\begin{figure}[H]

\begin{tikzpicture}[xscale=5]

\draw [thick] (-.2,-.1) node[below]{$X^{ON,1}_{c,t_0,s_1}$} -- (-.2,0.1);
\draw[-][draw=green, ultra thick] (-.2,0) -- (2.3,0);
\draw [thick] (2.3,-.1) node[below]{$X^{OFF,1}_{c,t_{end},t_{end},s_1}$} -- (2.3,0.1);

\node[above] at (-.2,0) {$t_0$};
\node[above] at (2.3,0) {$t_{end}$};

\node[above] at (1,0) {on };

\end{tikzpicture}
\end{figure}

\item[•] $\sum_{s\in S}( \gamma_{c,s}+\eta_{c,s}) = 0$: seafarer $c$ is off board at time 0\\

\item \label{C_on,off,on} $ $%\\
\begin{figure}[H]

\begin{tikzpicture}[xscale=5]
\draw [draw=white] (-.25,0) -- (-.1,0);
\draw[-][draw=blue, ultra thick] (-.1,0) -- (.5,0);
\draw[-][draw=green, ultra thick] (.5,0) -- (1.3,0);
\draw[-][draw=teal, ultra thick] (1.3,0) -- (1.8,0);
\draw[-][draw=orange, ultra thick] (1.8,0) -- (2.3,0);


\node[above] at (-.1,0) {$t_0$};
\node[above] at (0.5,0) {$t'$};
\node[above] at (1.3,0) {$k''$};
\node[above] at (1.8,0) {$t'''$};
\node[above] at (2.3,0) {$t_{end}$};

\draw [thick] (-.1,-.1)-- (-.1,0.1);
\draw [thick] (0.5,-.1) node[below]{$X^{ON,1}_{c,t',s_1}$} -- (0.5,0.1);
\draw [thick] (1.3,-.1) node[below]{$X^{OFF,1}_{c,t'',k'',s_1}$} -- (1.3,0.1);
\draw [thick] (1.8,-.1) node[below]{$X^{ON,2}_{c,t''',s_2}$} -- (1.8,0.1);
\draw [thick] (2.3,-.1) node[below]{$X^{OFF,2}_{c,t_{end},t_{end},s_2}$} -- (2.3,0.1);
\node[above] at (0.2,0) {off};
\node[above] at (0.9,0) {on};
\node[above] at (1.6,0) {off};
\node[above] at (2,0) {on};
\end{tikzpicture}
\end{figure}


\item \label{C_on,off}$ $%\\
\begin{figure}[H]

\begin{tikzpicture}[xscale=5]
\draw [draw=white] (-.25,0) -- (-.1,0);
\draw[-][draw=blue, ultra thick] (-.1,0) -- (.66,0);
\draw[-][draw=green, ultra thick] (.66,0) -- (1.6,0);
\draw[-][draw=teal, ultra thick] (1.6,0) -- (2.3,0);

\node[above] at (-.1,0) {$t_0$};
\node[above] at (.66,0) {$t'$};
\node[above] at (1.6,0) {$k''$};
\node[above] at (2.3,0) {$t_{end}$};

\draw [thick] (-.1,-.1) -- (-.1,0.1);
\draw [thick] (0.66,-.1) node[below]{$X^{ON,1}_{c,t',s_1}$} -- (0.66,0.1);
\draw [thick] (1.6,-.1) node[below]{$X^{OFF,1}_{c,t'',k'',s_1}$} -- (1.6,0.1);
\draw [thick] (2.3,-.1)  -- (2.3,0.1);
\node[above] at (0.33,0) {off};
\node[above] at (1,0) {on};
\node[above] at (2,0) {off};
\end{tikzpicture}
\end{figure}



\item \label{C_on} $ $%\\
\begin{figure}[H]

\begin{tikzpicture}[xscale=5]
\draw [draw=white] (-.25,0) -- (-.1,0);
\draw[-][draw=blue, ultra thick] (-.1,0) -- (1,0);
\draw[-][draw=green, ultra thick] (1,0) -- (2.3,0);

\node[above] at (-.1,0) {$t_0$};
\node[above] at (1,0) {$t'$};
\node[above] at (2.3,0) {$t_{end}$};

\draw [thick] (-.1,-.1) -- (-.1,0.1);
\draw [thick] (1,-.1) node[below]{$X^{ON,1}_{c,t',s_1}$} -- (1,0.1);
\draw [thick] (2.3,-.1) node[below]{$X^{OFF,1}_{c,t_{end},t_{end},s_1}$} -- (2.3,0.1);
\node[above] at (0.5,0) {off };
\node[above] at (1.7,0) {on };
\end{tikzpicture}
\end{figure}


\item \label{C_never_on} $ $%\\
\begin{figure}[H]

\begin{tikzpicture}[xscale=5]
\draw [draw=white] (-.25,0) -- (-.1,0);
\draw[-][draw=blue, ultra thick] (-.1,0) -- (2.3,0);

\node[above] at (-.1,0) {$t_0$};
\node[above] at (2.3,0) {$t_{end}$};


\draw [thick] (-.1,-.1) -- (-.1,0.1);
\draw [thick] (2.3,-.1) -- (2.3,0.1);

\node[above] at (1,0) {off };
\end{tikzpicture}
\end{figure}

\end{enumerate}




\item[\textcolor{green}{•}] Constraint for the on-board period of shift 1.\\

\begin{equation}
\begin{split}
\hspace{35mm} MinOn_{r(c)} \cdot  \sum_{s\in S}& \sum_{t\in T^s}  \sum_{k\in H^t_{c,s}} X^{OFF,1}_{c,t,k,s}   \\ &  \leq \overbrace{ d(t^{OFF,1}_c)- \left[ d(t^{ON,1}_c) - \sum_{s\in S}\gamma_{c,s} \cdot t^W_c \right]}^{time\;spent\; on-board} \\ & \leq \sum_{r\in R} \rho_{r,c} \cdot MaxOn_r \hspace{40mm} \forall c\in C \label{C_ON, shift1}
\end{split}
\end{equation}
Time spent on-board by seafarer $c$ must be included in the range between $MinOn_{r(c)}$ and $MaxOn_{r(c)}$. However, it can happen (case \ref{C_on}) that $c$ remains on-board after the end of the time horizon and at time $t_{end}$ he has not spent on-board the minimum time. For this reason, in case  $\sum_{s\in S} \sum_{t\in T^s}  \sum_{k\in H^t_{c,s}} X^{OFF,1}_{c,t,k,s}=0$, we set the lower bound as 0 .\\ The central part of the inequality refers to the time spent on board by seafarer $c$ and it is an expression which gathers both the cases $\sum_{s\in S}\gamma_{c,s}=1$ and $\sum_{s\in S} \gamma_{c,s}=0$. Without constraint \eqref{C_dummy Xon,1}, which guarantees that $d(t^{ON,1}_c)=0$ if $\gamma_{c,s}=1$ for some $s$, we would have two different constraints:
\begin{align*}
MinOn_{r(c)} \cdot \left(   \sum_{s\in S} \sum_{t\in T^s}  \sum_{k\in H^t_{c,s}} X^{OFF,1}_{c,t,k,s}  \right) & \leq d(t^{OFF,1}_c)-  d(t^{ON,1}_c)  \\ & \leq MaxOn_{r(c)} \hspace{40mm} \forall c\in C: \sum_{s\in S}\gamma_{c,s}=0 \\
MinOn_{r(c)} \cdot \left(   \sum_{s\in S} \sum_{t\in T^s}  \sum_{k\in H^t_{c,s}} X^{OFF,1}_{c,t,k,s}  \right) & \leq d(t^{OFF,1}_c) + t^W_c \\ &\leq MaxOn_{r(c)}  \hspace{40mm}  \forall c\in C : \sum_{s\in S}\gamma_{c,s}=1
\end{align*}

In order to understand the two inequalities, it can be useful to refer to cases \ref{C_off,on,off} and \ref{C_on,off,on}. \\ If $\gamma_{c,s}=1$ for a $s\in S$, the time spent on ship $s$ during shift 1 is equal to the sum of the period already spent on board before time $t_0$, such as $t_c^W$, and the time between $t_0$ and the moment of going off-board, such as $d(t^{OFF,1}_c)$.\\
On the other hand, if $c$ is off board at time $t_0$, then the period spent on board is equal to the time elapsed between $d(t^{ON,1}_c)$ and $ d(t^{OFF,1}_c) $. Moreover, if $\sum_{s\in S}\gamma_{c,s}=0$ but $ \sum_{s\in S}\eta_{c,s}=0$, then the constraint is $0\leq d(t^{OFF,1}_c)-  0 \leq \bar{h}_{r(c)}$.



\item[\textcolor{orange}{•}] Constraint for the on-board period of all the other shifts 
\begin{equation}
\begin{split}
\hspace{20mm} MinOn_{r(c)} \cdot \sum_{s\in S} \sum_{t\in T^s}  \sum_{k\in H^t_{c,s}} & X^{OFF,i}_{c,t,k,s} \\ & \leq  d(t^{OFF,i}_c)-  d(t^{ON,i}_c)  \\ & \leq  MaxOn_{r(c)}  \hspace{30mm} \forall c\in C, i\in I\setminus\{1\}
\label{C_onboard_other shifts}
\end{split}
\end{equation}

Constraint \eqref{C_onboard_other shifts} can be compared to the previous one in the case $\sum_{s\in S} \gamma_{c,s}=0$.


\item[\textcolor{blue}{•}] Constraint for the off-board period of a shift whose holiday period has begun before the beginning of the time horizon
\begin{equation}
\left(\pi_{r(c)}\cdot (t^W_c+t^h_c)-v\right) \cdot \sum_{s\in S}\sum_{t\in T^s} X^{ON,1}_{c,t,s}   \leq \overbrace{t^H_c +d(t^{ON,1}_c) }^{time\;spent\;off\;board}  \qquad\qquad \forall c\in C:\,t^H_c>0
\label{C_off_shift1_tHc>0}
\end{equation}

This constraint refers to cases from \ref{C_on,off,on} to \ref{C_never_on}: seafarer $c$ is on holiday at time $t_0$, after having been on board for $t_c^W+t_c^h$ time units and having already spent $t_c^H$ time units on holiday. \\ We have to ensure that $c$'s holiday period (which is given by the sum of $t_c^H$ and the time elapsed between time $t_0$ and the moment he starts a new shift, $d(t^{ON,1}_c)$) is at least equal to $\pi_{r(c)}$ times the period he has spent on board, minus the allowed violation. \\
Nevertheless, it can happen (case \ref{C_never_on}) that $c$ never starts his shift 1 in the considered time horizon, and so $ \sum_{s\in S} \sum_{t\in T^s} d(t,s) \cdot  X^{ON,1}_{c,t,s} =0$. In this case we set the lower bound equal to 0.

\item[\textcolor{yellow}{•}] Constraint for the off-board period of shift 1 whose working period or the handover period have begun (but not ended) before the beginning
\begin{equation}
\begin{split}
\overbrace{\pi_{r(c)}\cdot \left( t_c^W+t_c^h+d(t^{OFF,1}_c) \right)-v }^{minimum\;required\;off-board\;period}-M_1(r(c)) \left(1-\sum_{s\in S}\sum_{t\in T^s}  X^{ON,2}_{c,t,s}\right)\leq d(t^{ON,2}_c)- d(t^{OFF,1}_c) \\ \qquad \forall c\in C: \sum_{s\in S}(\gamma_{c,s}+\eta_{c,s})=1
\end{split}
\label{C_off_shift1_gamma=1}
\end{equation}


This constraint refers to cases \ref{C_off,on,off}, \ref{C_off,on} and \ref{C_off}: seafarer $c$ has started working in a time preceding $t_0$. For example, in case $\gamma_{c,s}=1$ for some $s\in S$, at time $t_0$ seafarer $c$ has been working for $t_c^W$ time units and at a certain time during the time horizon, he goes off board. \\
Constraint \eqref{C_off_shift1_gamma=1} ensures that $c$ could spend at least the minimum period on holiday, before starting a new shift.\\
If shift 2 starts during the time horizon, then the holiday period is the time elapsed between shift 1 going off board ($d(t^{OFF,1}_c) $) and shift 2 going on board ($d(t^{ON,2}_c)$). It must be greater or equal to $\pi_{r_c}$ times the period spent on board: $t_c^W+t_c^h$ plus the time elapsed from time $t_0$ and shift 1 going off board.\\
If $c$ stays on board until the end of the time horizon, we can not ensure the period spent on holiday to be at least the minimum required. For this reason we introduce a big-M constraint. Definition of $M_1(r(c))$  depend on seafarer's rank, but it is analogue to $M_1$ in constraint \textcolor{red}{\eqref{off_shift1_gamma=1}} of the simplified model: $M_1(r(c)):= (1+\pi_{r(c)}) \cdot  MaxOn_{r(c)} -v$.



\item[\textcolor{teal}{•}] Constraint for the off-board period of all the other shifts (shifts whose working part begins and ends during the time horizon)
\begin{equation}
\begin{split}
\pi_{r(c)} \cdot \left( d(t^{OFF,i}_c) - d(t^{ON,i}_c) \right) - v  -M_2(r(c))\cdot \left(1-\sum_{s\in S}\sum_{t\in T^s} X^{ON,i+1}_{c,t,s}\right) \leq  d(t^{ON,i+1}_c) - d(t^{OFF,i}_c) \\ \forall c\in C,\forall i\in I, i\geq \sum_{s\in S}(\gamma_{c,s}+\eta_{c,s})+1 
\end{split}
\label{C_off_othershifts}
\end{equation}
So far we have described constraints for the off-board period of shifts whose start is before $t_0$. For all the other shifts, such as $i\geq 2$ if $\sum_{s\in S} \gamma_{c,s}=1$ or $\sum_{s\in S} \eta_{c,s}=1$,  and $i\geq 1$ if $\sum_{s\in S}( \gamma_{c,s}+\eta_{c,s})=0$, the constraint structure is similar to the one of \eqref{C_off_shift1_gamma=1}, with the difference that the on-board period of the considered shift $i$ is given by  $d(t^{OFF,i}_c) - d(t^{ON,i}_c)$. \\ On the other hand, off-board period is defined by the time elapsed from shift $i$ going off moment and shift $i+1$ going on moment. If seafarer $c$ does not start a new shift $i+1$ within the end the time horizon, we can not be sure that the holiday period is at least equal to the minimum required. For this reason we introduce $M_2(r(c))$, which is defined like $M_2$ in the simplified model: \\$M_2 := \pi_{r(c)} \cdot MaxOn_{r(c)} + L -v$.\\

The next two constraints guarantee that on every ship there is always the required number of experienced seafarers




\item[•] Enough experienced in the rank seafarers 
\begin{flalign}
& \hspace{38mm}\sum_{c\in C} Y_{c,t,t+1,s} \cdot  \varepsilon_{c,u} \geq n(u) &\forall s\in S,\, \forall t\in T^s,\,\forall u\in U \label{C_combined_rank}
\end{flalign}

\item[•] Enough experienced in the company seafarers
\begin{flalign}
& \hspace{38mm}\sum_{c\in C} Y_{c,t,t+1,s} \cdot  \tilde{\varepsilon}_{c,u} \geq n(u) & \forall s\in S,\, \forall t\in T^s,\,\forall u\in U \label{C_combined_company}
\end{flalign}

In the left side of both inequalities we add a `1' for every seafarer who satisfies both the following conditions: 1) is on ship $s$ between time $t$ and $t+1$ ($Y_{c,t,t+1,s}=1$); 2) he is experienced according to rule $u$ ($\varepsilon_{c,u}=1$).\\





\end{enumerate}





\subsubsection*{Objective function}
Sum of
\begin{itemize}
\item[•] \textit{Costs of flights for outward journey} 
\begin{equation*}
\sum_{c\in C} \left[ \sum_{s\in S}  \sum_{i \in I} \sum_{t\in T^s}  X^{ON,i}_{c,t,s} \cdot f_{p(t,s),a(c)} \right]
\end{equation*}

\item[•] \textit{Costs of flights for return journey}
\begin{equation*}
\sum_{c\in C} \left[\sum_{s\in S}\sum_{i \in I} \sum_{t\in T^s\cup\{t_0,t_{end}\}} \sum_{k\in H^t_ {c,s}} X^{OFF,i}_{c,t,k,s}  \cdot f_{p(k,s),a(c)} \right]
\end{equation*}


\end{itemize}

\subsection*{Size of the model}
In this section we comment the size of the model and the number of variables.
\subsubsection*{Number of variables}
\begin{tabular}{lll}
\toprule
Variable & Definition sets & Number of variables \\
\midrule
$X^{ON,i}_{c,s,t}$ & $\forall i\in I, \, c\in C,\, s\in S,\, t\in T^s$ & $ |I|\cdot |C| \cdot \sum_{s\in S} \left( |T^s| + 1\right)$ \\\\
$X^{OFF,i}_{c,s,t,k}$ & $ \forall i\in I, \, c\in C,\, s\in S,\, t\in T^s,\, k\in H^t_{c,s} $ & $|I|\cdot \sum_{c\in C} \sum_{s\in S} \left[ \left(|T^s|+1\right)\cdot |H^t_{c,s}| \right]$ \\\\
$Y_{t,t+1,s}, Y_{c,t^s_{max}.t^s_{max},s}$ & $ c\in C,\, s\in S,\, t\in \{t_0\}\cup T^s\setminus\{t^s_{max}\}$ & $|C|\cdot \sum_{s\in S}(|T^s|+1)$ \\
\bottomrule\\
\end{tabular}

We can conclude that the number of variables is $\mathcal{O}\left(|I|\cdot \sum_{c\in C} \sum_{s\in S} \left[ \left(|T^s|+1\right)\cdot |H^t_{c,s}| \right]\right)$

\subsubsection*{Number of constraints}

\begin{tabular}{lll}
\toprule
Constraint \hspace{15mm} & Definition sets \hspace{40mm} & Number of constraints \\
\midrule
  \eqref{C_flow_node_0}    & $\forall c\in C,\,s\in S $ & $|C|\cdot |S|$         \\\\
 \eqref{C_flow_nodes_c}    &  $\forall c\in C $      &  $|C|$ \\\\
 \eqref{C_flow_nodes_t}    &  $ \forall c\in C,\, s\in S,\, t\in T^s$      & $|C|\cdot\sum_{s\in S} |T^s|$        \\\\
 \eqref{C_flow_node_t*}    &  $\forall c\in C,\, s\in S$    &        $|C|\cdot |S|$ \\\\
 \eqref{C_number_crewmembers_end_escluded} +  \eqref{C_number_crewmembers_end}     &   $\forall s\in S,\, t\in T^s, r\in R $    &   $|R|\cdot \sum_{s\in S} |T^s|$      \\\\
 \eqref{C_at_most_one_on_off}    &  $\forall i\in I,\,c\in C$      & $|I|\cdot |C|$        \\\\
 \eqref{C_off_must_follow_on}    & $\forall c\in C,\,i\in I,\,s\in S $  &  $|C|\cdot |I| \cdot |S|$        \\\\
 \eqref{C_shift_order}    & $\forall c\in C,\, i\in I\setminus\{1\}$       &   $|C|\cdot (|I|-1)$     \\\\
 \eqref{C_dummy Xon,1}    & $\forall c\in C,\,s\in S $       &        $|C|\cdot |S|$ \\\\
 \eqref{C_ON, shift1}    &   $\forall c\in C$     &    $|C|$     \\\\
 \eqref{C_onboard_other shifts}    &  $\forall c\in C,\, i\in I\setminus\{1\}$      &   $|C|\cdot (|I|-1)$      \\\\
 \eqref{C_off_shift1_tHc>0} +  \eqref{C_off_shift1_gamma=1}   & $\forall c\in C: \, t_c^H>0 \, \vee \, \sum_{s\in S}( \gamma_{c,s}+\eta_{c,s})=1$        &   $\leq |C|$      \\\\
 \eqref{C_off_othershifts}    &  $\forall c\in C,\, i\in I:\,i\geq 1+\sum_{s\in S} ( \gamma_{c,s}+\eta_{c,s})$      &    $\leq |C|\cdot |I|$     \\\\
 \eqref{C_combined_rank}    &   $\forall s\in S,\ t\in T^s,\,u\in U$     &    $|U|\cdot \sum_{s\in S} |T^s|$     \\\\
 \eqref{C_combined_company}    &   $\forall s\in S,\ t\in T^s,\,u\in U$     &    $|U|\cdot \sum_{s\in S} |T^s|$       \\
\bottomrule\\
\end{tabular} 

We can observe that $|S|\cdot |I| \leq \sum_{s\in S} |T^s|$. In fact, every shift is defined by one or two time moments, and so the number of shifts can not be greater than the number of times a ship stops. Moreover, $|R|<|C|$, since for every rank there is a group of seafarers belonging to that rank.\\
Thanks to these observations, we can group elements of the third column into two categories: \\$\mathcal{O}\left(|C|\cdot \sum_{s\in S} |T^s|\right)$ and $ \mathcal{O} \left( |U|\cdot \sum_{s\in S} |T^s| \right)$. Then the complexity is $ \mathcal{O} \left( (|C|+|U|)\cdot \sum_{s\in S} |T^s| \right)$. \\
However, according to our data, the set $U$ of the rules for combinations of seafarers' ranks, is really small compared to C. So, in practice, it is the term $\mathcal{O}(|C| \cdot \sum_s |T^s|)$ that will be dominating.\\

\textit{Given the complexity of this model, we prefer to formulate the problem in a different way, in order to make it more suitable for the implementation. In the next chapter, we approach the problem as a set partitioning (?) problem}


\subsection*{About the planning horizon}
In the two models described in this chapter we have considered a time horizon with a fixed length and we have supposed to re-optimize the crew schedule at the end of it.\\

Nevertheless, it might be preferable, especially from the seafarers' point of view, to know in advance when they will go off board, once they have started working.\\
For this reason, we introduce a re-optimization interval of $\lambda$ time units and we consider $L=\lambda+MaxOn$.
This assumption allows us to be sure that if we schedule a shift for a seafarer and it starts before the next re-optimization, then we know when it will finish. Hence, at the moment of the following re-optimization, we already know that he will be on board until a certain time defined in advance.\\
On the other hand, if we initially schedule a shift for a seafarer and this shift starts after the period $\lambda$, then, in one of the following re-optimization phases we still have time to change mind and schedule a different shift for the seafarer. \\ As a consequence of this, we can have fractional shifts only at the end of the planning horizon.\\

This way of planning allows us to take into consideration what happens `in the future', such as after the re-optimization period, and hence to take better decision in the present.\\

As we said in chapter \textcolor{red}{2 (problem description)},  the previous models are a generalization of what we have described now: if we already know,at the beginning of the time horizon, when a shift will finish, then variables $X^{OFF,1}_{c,t',k',s'}$ (and as a consequence also $Y_{c,t,t+1,s'}$ for $t< t'$) are fixed for every seafarer $c$ who is already on board on ship $s'$.





\end{document}
