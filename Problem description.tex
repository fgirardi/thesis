\documentclass[11pt,a4paper,oneside]{article}
\linespread{1.2}   
  %pacchetti indispensabili
  \usepackage{amsmath,amscd,amsfonts,amssymb}
  \usepackage[english]{babel}
  \usepackage[utf8]{inputenc} %attenzione codifica UTF8
  \usepackage[a4paper,top=2cm,bottom=2.5cm,left=2cm,right=2cm]{geometry}
  \usepackage{graphicx}  
  \usepackage{float}
  \usepackage{hyperref}
  \usepackage{color}
  \usepackage{xcolor}
  \usepackage{tikz}
  \usepackage{enumitem}
  \usetikzlibrary{decorations.pathreplacing}


%%to write date/time in the page
\usepackage{fancyhdr}
\usepackage{datetime}
\fancyhead[R]{\scriptsize{\today\ \currenttime}}
\pagestyle{fancy}


  
  \begin{document}

\section*{Problem Description}
\textit{[Our problem]} The Liner Shipping Crew Scheduling Problem (LSCSP) addresses the problem of assigning seafarers to cargo vessels, given the liner shipping network and corresponding service schedules. In particular, we consider the objective of minimizing the costs for moving crew from their homes to the ports where they sails, and the return journey, while respecting vessel specific crew requirements on each ship. These costs include, for example, flights, hotels, ground transportation fares. \\

\textit{[brief description of liner shipping service (different services, one visit per week at every port...):]}
Each liner shipping service represents a cyclic shipping route that starts at some port, visits other ports and return to the original port. A sailing between two ports is also referred to as a \textit{leg}.\\
The duration of a single round trip can last several weeks and vessels operate in ports on a service with a fixed frequency, typically weekly. As a consequence, the number of vessels operating a service usually equals the duration of the service in weeks: if a round trip takes 3 weeks, for example, 3 vessels would be operating on the service in a distance of one week.  \\
Therefore, these liner shipping services follow fixed schedules and the weekly arrival and departure times at ports are published. For this reason, a liner shipping network can be compared to a bus network, with bus routes resembling services and passengers corresponding to containers.\\


\textit{[requirements on every ship]}
 Every ship has requirements about the composition of the crew. First of all, every role must be covered, and so there are constraints regarding the number of seafarers per \textit{rank}  (e.g. captains, officers - chief officer, 1st officer, 2nd officer  -, engineers etc). Moreover, it is important to have some experienced seafarers on board. The experience of every seafarer is divided into \textit{experience in the rank} and \textit{experience in the company}. There are some combinations of ranks for which at least one of the seafarers on board belonging to these ranks must have a \textit{minimum required experience} (in the company or in the rank).\\\\
Our assumptions are based on real data provided by Maersk Line. In these data, we have a big pool of seafarers of different rank, experience and nationality. \\

Seafarers' working periods are subject to some rules.\\
\textit{[On/off board periods, shift:]}
 According to their rank, seafarers must work for a period and then they have a period of holiday. We suppose that seafarers are allowed to leave a ship only when it stops at some ports, even though in reality there may be some emergency cases in which they are picked up by a helicopter while the ship is at sea.\\\\
\textit{[Handover period:]} 
When a seafarer leaves a ship, an other crew member of the same rank must take his place. However, the outgoing and the incoming seafarers must spend a period together on board (the length of this period is based on rank and must be included in a proper range). This is called \textit{handover period}. On the other hand, by \textit{working period} we mean the period which starts when a seafarer sails on a ship and it ends when the handover period begins.\\
For example, when a captain finishes working on a ship and an other captain takes his place, the two must spend at least 5 days together on the ship before the outgoing one definitively leaves the ship. \\\\
\textit{[Definitions]}  The union of a working period and the respective handover period composes the period that a seafarer spends on a ship, for this reason we call it \textit{on-board period}. Every {on-board period} must be followed by a period of holiday (also referred to as \textit{off-board period}). 
We refer to the union of on-board and off-board periods as \textit{a duty period}. During a duty period, the on-board period must be carried out without interruptions and on the same ship.\\
The minimum duration of an off-board period is identified by a fraction of the respective on-board period.  This \textit{ratio} depends on seafarer's rank. However, we allow a little violation for minimum off-board period duration without penalizing it with a cost.\\ We do not consider an upper bound for off-board period duration: this means that a seafarer can be employed only for some months and then dismissed.\\
Senior ranks seafarers, for example captains or chief engineers, typically have a range for on-board period of about 2-4 months, with a ratio for minimum off-board period equal to 1. On the other hand, seafarers with less experience are likely to work from 3 up to 6 months and their off/on board periods ratio is equal to 0.5. \\\\
The period which we have called \textit{on-board period} is typically named in literature as \textit{a shift}. In the following we use both the words indistinctly, even though we usually use the first one when we want to contrast the period spent on board to the off board one.\\

\textit{[Objective]}: As we already said, this model aims to assign crew to the ships respecting the above constraints and minimizing costs which occur when seafarers move from their homes to the ports where they sail and the return journey (which can happen from a different port). Such costs can be divided into ``air fares" (flight costs) and ``non-air fares" (hotels, ground transportation, port agent...). \\\\
Let us consider a very simple example which helps to understand the problem.
We take into consideration the route showed in the figure \ref{example_route}, only one ship and the requirement of one seafarer on board. Moreover we suppose to have a pool of some European and American seafarers.\\
\begin{figure}[h]
\centering
\includegraphics[scale=0.55]{routenet_example.pdf}
\caption{Example of a real liner shipping route}
\label{example_route}
\end{figure}
\vspace{0mm}
Let us suppose that a New York captain is working on the ship and after the minimum on-board period he is in Felixtowe. Flying from England to his home town can cost several hundred of euros. For this reason, it might be preferable to let him stay on the ship for 11 days more and go off board in Newark. Furthermore, in order to satisfy the handover period, we can assign a French captain to the ship starting from Le Havre: in this way we ensure that the two captains spend 7 days together on the ship, and so the handover period is satisfied.\\



\textit{[Remark about the objective:]} We must underline that this model is not intended to reduce seafarers' pool size. This means that we can ignore the wages, since seafarers are paid regardless of whether they are used or not, and consider a model for minimizing seafarers' transfer costs from ports to home-airports and vice-versa.\\


%\textit{[Some data:]} Our tests are executed on real data provided by Maersk Line.\\
%Maersk Line has to schedule the crew for around 250 vessels. The problem can, however, be decomposed by vessel type, as different vessel types may have different manning requirements and not all seafarers can be assigned to all vessel types. The largest data instance will consist of around 40-50 vessels and 2000 seafarers. A service can consist of up to 20 legs and the sailing time of each leg can equal up to several days. \\



\textit{[Time horizon:]} 
A final remark concerns the modelling of time. Since liner shipping schedule is known well in advance, we can suppose to have a very long term ship schedule and every time we optimise the crew scheduling for a period called \textit{time horizon}, which has fixed length.\\ Furthermore, we can also consider a \textit{re-optimization interval} of time. In other words, we can solve the problem for the current time horizon, but then we repeat the optimization after a short period. In this way we have a better control of what happens in the future (such as after the end of the re-optimization period) and hence we can take better decisions in the present.\\\\
In the compact formulation we present a model in which the re-optimization interval corresponds to the whole time horizon. However, as we describe \textcolor{red}{in section XXX}, this case is a generalization of the one with a re-optimization period shorter than the time horizon.\\
Then, in the \textcolor{red}{set partitioning model} formulation, we consider a more realistic case, with a re-optimization period shorter than the time horizon.
Further details are given in the respective chapters.



\newpage

\section*{Literature review}

\textit{[In general, few applications for crew scheduling problem at sea. Motivations according to Christiansen et al and Brouer et al.]} \\
To our knowledge, there are no other papers that directly address crew scheduling for the liner shipping industry. Actually, the crew scheduling problem at sea has received little attention by the OR community (Yuan, 2016), regardless of the type of ship. Christiansen et al (2007) provide an excellent overview on maritime transportation and give some reasons of the scarcity of applications of OR in maritime transportation planning problems (of which crew scheduling is part), contrasting it to the size and the possible impact of optimization within the industry.\\ Among others, they underline the low visibility of the shipping industry (especially cargo) in respect to other ground transportation modes. Moreover, they point to the long tradition of the ocean shipping industry, which hence may tend to be more conservative and not open to new ideas. \\
On the same line, Brouer et al (2014) believe that ``the lack of OR within liner shipping is partly because of barriers for new researchers to engage in the liner-shipping research community, since constructing mathematical models and creating data for computational results requires profound knowledge of the domain and data sources". \\


\textit{[airline crew scheduling]}:\\
On the other hand, ``in the airline industry the crew scheduling problem is a large, complex problem that has received a significant amount of attention over the past 30 years" (Giachetti et al., 2013).
Gopalakrishnan and Johnson (2005) give an exhaustive description of airline crew scheduling. The problem is usually solved in two stages: \textit{pairing generation} and \textit{optimization}.
The first one is intended to create pairings, such as sequences of flights that begin and end at a crew base and such that in a sequence the arrival city of a flight coincides with the departure city of the next flight. These pairings must be feasible, such as they must satisfy some constraints (for example allowing a minimum number of hours of rest between duties or not exceeding the maximum allowed working period).\\ ``In reality it may not be possible to generate all legal pairings apriori since there are too many of them. A column generation scheme is usually employed for the implicit generation of legal pairings".\\
Once pairings have been generated, it is necessary to solve the resulting set-partitioning problem in order to find a set of minimal cost crew pairings covering all flight legs. \\
The authors also describe ``three approaches to pairing generation" and some ``successful solution methodologies".\\\\


\textit{[difference: airlines vs liner shipping]}\\
Even though some similarities between crew scheduling for airlines and liner ships can be found, there is a significant difference: once a seafarer is assigned to a ship, he stays on that ship until the end of the end of his working period. On the other hand, in air short and medium haul, a working period typically consists of several tasks to be carried out on different flights (Kohl and Karisch, 2004). \\
We can notice that this difference does not concerns only liner ships, but all types of deep-sea vessels, according to the definition given by Christiansen et al. (2007). \\
In the following, we give an overview of some relevant publications regarding crew scheduling for deep-sea vessels.\\




%Moreover, in airline scheduling, if it is not possible to generate a pairing with the same airport for departure and arrival, an employee can be assign to a flight as a passenger. This type of flight is called a \textit{deadhead} (Gopalakrishnan and Johnson, 2005). In case of liner shipping, deadheading is not a solution: the length of time taken to travel by a ship is significantly more than if a flight is taken, so this long period spent on-board without being employed would not be considered by seafarers as part of the holiday period, but neither is part of the working one. \\
%An other difference concerns the handover period: in liner shipping we must ensure that every time a seafarer finishes his working period and someone else takes his place, the two spend a minimum period together on the ship. On the other hand, in airline crew scheduling we can not have a constraint which regards outgoing and incoming pilots on the same flight.\\






\textit{[A few articles for deep-sea vessels problems:]}\\
John et al (2013) wrote one of the first articles about a deep-sea vessel crew scheduling problem. They focus on a long term planning with a time frame of one year. Even though they consider constraints very similar to ours, one relevant difference between their work and the one presented in this thesis is that they do not suppose to have a ship schedule available for long term. Moreover, their objective is to minimize the pool of seafarers. \\ For this reasons, they determine the start and end dates of contract periods (which include the on-board period and the required travel time) and assign them to the seafarers, without knowing in advance the exact port where they will start their journey. As a consequence, their model can not consider travel costs.\\ The paper present a mathematical model formulation, \textcolor{red}{but the solution method is left as a future research \textit{(how can I say it in a way that it doesn't seem that I am accusing them for not having done it?)}}
\\

Leggate (2016) focuses on the crew scheduling problem faced by a large, global company providing support services in the offshore oil industry. He presents two mathematical models: the first one is relatively simple to solve with standard techniques, but makes many simplifying assumptions; the second one is much more realistic, but requires a more tailored solution approach. In both cases there is no partial schedule at the start of the planning process: all schedules are constructed from scratch. Moreover, ``both of these models can be likened to the crew recovery problems seen in other transportation scheduling literature, and can be solved with the objective of minimizing either the number of changes from the existing schedule or the cost of these changes". In the end, a heuristic approach is proposed. \\
The main difference between this problem and LSCSP is that in liner shipping routes, timings and crew demand are defined in advance.\\




Finally, Giachetti et al (2013) present an ``Optimization-based decision support system for crew scheduling in the cruise industry".\\ They focus on the minimization of costs for moving crew members from their home cities to the cruise ship's departure port. However, even though the objective is almost the same of our problem, there is a great number of differences in the assumptions.\\
First of all, according to our data, every liner ship needs at most 10 seafarers, while ``a single cruise ship can have between 1000 and 1500 crew members, and the largest cruise ship, operated by Royal Caribbean Cruise Lines, has 2500 crew members".\\
In our case, we suppose that seafarers fulfil their work without unexpected events occurring. However, the number of employees required by a cruise ship is too big for not considering unplanned factors, such as unplanned crew movements that can cause the number of crew on board to vary, for example delayed flights or medical leave. For this reason, Giachetti et al. start their scheduling system with a crew demand planning module and then assign crew to the ships overbooking the number of employee. \\\\
An other difference concerns the lengths of on and off-board periods (here they are called \textit{contract} and \textit{vacation} periods): they are equal for every employee, even though the employee contract length is allowed to vary by a couple of weeks so as to take advantage of lower journey costs, if possible.\\\\
Briefly, they describe a scheduling system that implements a two-stage planning process. Firstly, they determine overbooking levels for the number of crew to offer contracts to, then, second, they propose an integer programming formulation to minimize the movement cost of assigning crew to ships, while maintaining 
a desired level of service and a certain nationality mix of crew members on each ship. In the end, they use CPLEX to solve the problem and it results that their optimization-based decision support system ``can solve actual sized problems in a reasonable computational time".\\


\textit{[Conclusion]}
To sum up, we can see that for the deep-sea vessels problems studied in literature  a compact formulation is presented. For this reason, we start modelling our problem from the compact formulation. However, we will see that our model has a huge number of constraints and variables. This leads us to consider a different approach, inspired by the airline crew scheduling problem solution method: instead of pairings, for every seafarer we generate shifts (such as sequences composed by a working period, handover period and holiday period), and then we choose the shifts with minor costs.









\end{document}
