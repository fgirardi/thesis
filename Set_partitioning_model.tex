\documentclass[11pt,a4paper,oneside]{article}
\linespread{1.2}   
  %pacchetti indispensabili
  \usepackage{amsmath,amscd,amsfonts,amssymb}
  \usepackage[english]{babel}
  \usepackage[utf8]{inputenc} %attenzione codifica UTF8
  \usepackage[a4paper,top=2.25cm,bottom=2.25cm,left=2cm,right=2cm]{geometry}
  \usepackage{graphicx}  
  \usepackage{float}
  \usepackage{hyperref}
  \usepackage{color}
  \usepackage{xcolor}
  \usepackage{tikz}
  \usepackage{enumitem}
  \usetikzlibrary{decorations.pathreplacing}
  \usepackage{booktabs} % commands \toprule for tables


%%to write date/time in the page
\usepackage{fancyhdr}
\usepackage{datetime}
\fancyhead[R]{\scriptsize{\today\ \currenttime}}
\pagestyle{fancy}


  
\begin{document}
\section*{\textit{The set partitioning formulation}}


\textit{[Introduction: what we have done so far and we want to do]}\\
So far we have modelled LSCSP in a compact formulation. In this chapter we present an alternative formulation, similar to the ones which are used to solve airline crew scheduling problems.\\

\textit{[recall airline crew scheduling approach]}\\
As we have seen in the literature review (\textcolor{red}{chapter 3}), the typical approach for airline crew scheduling problems consists of two stages: \textit{pairing generation}  and \textit{optimization}. \\
In our case, we do not have to create pairings (``sequences of flights that begin and end at a crew base and such that in a sequence the arrival city of a flight coincides with the departure city of the next flight", Gopalakrishnan and Johnson, 2005), but \textit{duty periods}. These duty periods must be legal, such as they must respect the range for the working period and for the handover period, and the respective minimum holiday period must be as long as the minimum required. \\ Then, once duty periods have been generated, the ones which respect all crew requirements on each ship and minimize the total cost are chosen.\\

\textit{[duty periods for every seafarer]} Duty periods are created for every seafarer individually. This is necessary because the cost of a duty periods depends on seafarer's home airport. In particular, the cost is given by the cost of moving a seafarer from his home to the airport where he sails and the return journey, which can happen from another port.\\

\textit{[time horizon]}\\
Finally, we specify that the formulation of the problem presented in this chapter contains a slight difference from the compact formulation: in this model we assume that if a seafarer is on board at the beginning of the time horizon, then we know when he will finish his working period and go off board. A motivation for this assumption can be found in the \textcolor{red}{solution methods chapter.}\\
On the other hand, the models in the previous chapters are a generalization of this: if we know, at the beginning of the time horizon, that a seafarers $c$, who is already on board on a ship $s$, will finish working at time $t'$ and go off board at time $k'$, then variable $X^{OFF,1}_{c,t',k',s}$ is fixed (and as a consequence also variables $Y_{c,t,t+1,s}$ for $t < t'$ are fixed). \\


%Finally, we recall what we said \textcolor{red}{in section ??}: in this model we consider a time horizon whose length is given by the sum of the re-optimization period length ($\lambda$) and the maximum on-board period length ($MaxOn$). As a consequence, for every duty period which starts in the first $\lambda$ time units, we have that its end is determined within the end of the time horizon. Then, at the beginning of every time horizon (such as every time we re-optimize the schedule), for every duty period which has started previously, we know when working, handover and holiday periods finish.\\

The basic set partitioning model described below assumes that all feasible duty periods have been explicitly enumerated.\\


\subsection*{Idea of the model}
For every seafarer $c$ we define a set of possible duty periods, $\Omega_c$. To every element $j\in \Omega_c$ we associate a binary variable $X_j^c$: $X_j^c$ = 1 if duty period $j\in \Omega_c$ is chosen in the solution, 0 otherwise.\\

We recall that we defined $S$ the set of all ships $s$, while $T^s$ is the set of indexes of time moments in which vessel s visits some port ($T^s=\{1,\,2,\,...,\,t^s_{max}\}$). $t_0=0$ and $t_{end}$ refer to the start and end of the time horizon. $d(t,s)$ is the time elapsed between time 0 and time $t\in T^s$.\\

Now, for every $s\in S,\,t\in T^s\setminus \{t^s_{max}\}$ we can consider the interval between $t$ and $t+1$. This interval is referred to as a \textit{time period} and is indexed by $p$. We can notice that if $p$ refers to interval between $t$ and $t+1$, then $p=t$.\\ Given $t_0=0$ (starting time of the time horizon) and $t=1\in T^s$, the period between these two moments is called $p_0^s=0$. In the same way, the period between $t^s_{max}$ and $t_{end}$ is $p_{max}^s$.\\\\
 The set of all $p$ referred to $t\in T^s$, $p_0^s$ and $p_{max}^s$ included, is called $P^s$.\\


\begin{figure}[H]
\begin{tikzpicture}[xscale=2.6]

\draw[-][dotted, thick] (0,0) -- (1,0);
\draw[-][thick]       (1,0) -- (3,0);
\draw[-][dashed,thick] (3,0) -- (4,0);
\draw[-][thick]       (4,0) -- (5,0);
\draw[-][dotted,thick] (5,0) -- (6,0);

\footnotesize

\node[above] at (0,0.2) {$t_0=0$};
\node[above] at (1,0.2) {$t=1$};
\node[above] at (2,0.2) {$t=2$};
\node[above] at (3,0.2) {$t=3$};
\node[above] at (4,0.2) {$t=t^s_{max}-1$};
\node[above] at (5,0.2) {$t=t^s_{max}$};
\node[above] at (6,0.2) {$t_{end}$};

\draw [thick] (0,-.1) -- (0,0.1);
\draw [thick] (1,-.1) -- (1,0.1);
\draw [thick] (2,-.1) -- (2,0.1);
\draw [thick] (3,-.1) -- (3,0.1);
\draw [thick] (4,-.1) -- (4,0.1);
\draw [thick] (5,-.1) -- (5,0.1);
\draw [thick] (6,-.1) -- (6,0.1);

\node[below] at (0.5,-0.2) {$p_0=0$};
\node[below] at (1.5,-0.2) {$p=1$};
\node[below] at (2.5,-0.2) {$p=2$};
\node[below] at (4.5,-0.2) {$p=t^s_{max}-1$};
\node[below] at (5.5,-0.2) {$p^s_{max}=t^s_{max}$};
\end{tikzpicture}
\caption{Example of a set $T^s$ and the correspondent $P^s$}
\end{figure}


We need also to define the set $P$ of all time periods, but we can not simply define it as the union of all $P^s$, since everyone is equal to $\{1,2,...,p^s_{max}\}$.
So, let $\tilde{T}$ be the sorted union $\cup_{s\in S} \cup_{t\in T^s} \{d(t,s)\}$. We can define $T$ as the set of indexes of elements of $\tilde{T}$. $T=\{1,2,...,t_{max}\}$ .\\
$\forall \tau \in T$ the interval between $\tau$ and $\tau+1$ is a time period indexed by $p$.
Finally, the period between $t_0=0$ and $t=1\in P$ is $p_0$ and the one between $t_{max}$ and $t_{end}$ is $p_{max}$.\\
 The set of all these period indexes is $P$.\\
 
For all duty periods $j\in \Omega_c, c\in C$, we need two information: 
\begin{enumerate}
\item[•] If, according to duty period $j\in \Omega_c$, seafarer $c$ works on a ship $s$ during a period $p\in P^s$. In this case we say that a binary parameter $a_{jcp}^c$ is 1, 0 otherwise ($a_{jcp}^s$ is the analogous of variable $Y_{c,t,t+1,s}$ in the previous model). Moreover, we define $s(j,c)$ the ship where seafarer $c$ is working according to duty period $j\in \Omega_c$.
\item[•] If duty period $j$ `involves' period $p\in P$, such as period $p$ is part of duty period $j$ as working or handover period, or even if it is part of the minimum holiday period. In this case we say that $b_{jp}^c$ is 1.
\end{enumerate}
\vspace{0mm}
As we have said, in this model we suppose that if a duty period for seafarer $c$ is selected, at the beginning of the following time horizon we know if $c$ is working, finishing the handover period or spending the minimum holiday period. Moreover, we know how long it takes for him to complete the duty period. \\
Hence we can define the following parameters:
\begin{enumerate}
\item[•] $\alpha_{sp}^c$ = 1 if seafarer $c$ works on ship $s$ during period $p\in P^s$, according to a duty period selected previously, 0 otherwise
\item[•] $\beta_p^c$ = 1 if seafarer $c$ is working or spending the handover period or spending the minimum holiday, during period $p\in P$, according to a duty period selected previously
\end{enumerate}

Furthermore, we recall that in the compact formulation we are not supposed to know when a seafarers leaves a ship, given that he has started and not finished an on-board period before the beginning of the time horizon. This fact has implied that the objective function in the compact formulation includes the cost for the return journey for seafarers previously employed. \\
In order to have the same objective function in the two formulations, here we need to define a parameter for the costs of such return journeys. We call them $\gamma_c$.\\

Finally, duty periods which are chosen as solution must respect the following constraints:
\begin{enumerate}
\item Selected duty periods for the same seafarer can not overlap in time
\item Every ship must have the required number of seafarers on board in every moment
\item Rules regarding the combination of experienced seafarers must be satisfied
\end{enumerate}
%
%
%\subsubsection*{Cost of every duty period \textit{\textcolor{red}{(probably I should put it in the chapter where I describe the generation of duty periods)}}}
%In this section we present how the cost of every duty period can be computed. We define $k(j,c)$ the cost of duty period $j\in \Omega_c$ for seafarer $c$.\\
%Let $Q$ be the set of all ports where seafarers can sail and $A$ the set of airports, such as seafarers' home bases. $f_{qa}$ is the cost of the travel from port $q\in Q$ to airport $a \in A$ and vice-versa. $q(t,s)$ is the port visited by vessel $s$ at time $T^s$.  \\\\ 
%In all the cases, seafarer $c$ has to move one (cases \ref{W/2},\ref{t_c^W W H H},\ref{t_c^W W H H/2}) or two (cases \ref{WHH} and \ref{W H H/2}) times. Moreover we have to distinguish between the forward and the return trip cost. \\
%We define $on(j,c)$ as the time moment in which seafarer $c$ goes on board according to duty period $j\in \Omega_c$, $off(j,c)$ the off-board moment.  Let $s(j,c)$ be the ship where seafarer $c$ spends his working period according to duty period $j\in \Omega_c$.\\ 
%With reference to what we defined in the compact formulation (\textcolor{red}{see figure \ref{flow_conservation} in chapter 4}) we say that in cases \ref{t_c^W W H H} and \ref{t_c^W W H H/2}, $on(j,c)=0$ . So we can define $q(0,s)$ as a `dummy' port for every ship $s$ and $f_{q(0,s)a}=0 \; \forall a\in A$. In the same way, in case \ref{W/2} we have that $off(j,c)=t_{end}$ and the cost of the journey from a `dummy' port $q(t_{end},s(j,c))$ to an airport $a$ is constant for every airport.\\\\
%In all the other cases, $on(j,c)$ and $off(j,c)$ belongs to $T^{s(j,c)}$, and so the costs of the journeys are $f_{q(on(j,c),s(j,c)),a(c)}$, with $s(j,c)\in S,\,a\in A$ and $q(on(j,c),s(j,c)),q\in Q$.\\
%
%The mathematical formulation of costs $k(j,c),\,j\in \Omega_c,\,c\in C$, exploits the fact that if $p\in P^s$ is the index referred to the interval between two time moments indexes $t$ and $t+1$, $t\in \{t_0\}\cup T^s$, then $p=t$ .\\
%Then we have: $\forall c\in C, j\in \Omega_c$
%\begin{equation*}
%on(j,c):= min\{p\in P^{s(j,c)} | a_{jsp}^c=1\}
%\end{equation*}
%If $m:= max \{p\in P^{s(j,c)}| a_{jsp}^c=1\}\, < \, p^s_{max}$ \\ $$ off(j,c):=m+1$$ 
%Else
%$$off (j,c):=t_{end}$$
%Then $$k(j,c):= f_{p(on(j,c),s(j,c)),a(c)}+f_{p(off(j,c),s(j,c)),a(c)}$$
%
%
%

\subsection*{Definition of the set partitioning model}

\subsubsection*{Sets}
\begin{itemize}
\item[•] $C$ = seafarers
\item[•] $P^s$ = set of indexes of time periods for ship $s$
\item[•] $P$ = set of indexes of all time periods
\item[•] $R$ = set of ranks
\item[•] $S$ = ships
\item[•] $U$ = set of indexes for the rules of combination of ranks for which a minimum experience is required
\item[•] $\Omega_c$ = set of duty periods for seafarer $c$
\end{itemize}


\subsubsection*{Parameters}

\begin{itemize}
\item[•] $a^c_{jsp}$ = 1 if seafarer $c$ works on ship $s$ during period $p\in P^s$ according to duty period $j\in \Omega_c$, 0 otherwise
\item[•] $\alpha^c_{sp}$ = 1 if seafarer $c$ works on ship $s$ during period $p\in P^s$ according to a duty period chosen previously, 0 otherwise
\item[•] $b^c_{jp}$ = 1 if duty period $j\in \Omega_c$ involves period $p\in P$ as part of the working or handover or holiday periods.
\item[•] $\beta^c_{p}$ = 1 if a duty period chosen previously involves period $p\in P$ as part of the working or handover or holiday periods.
\item[•] $\gamma_c$ = cost of the return journey for a seafarer who has been employed previously and is still on board at the beginning of the time horizon
\item[•] $d(p,s)$ = time elapsed between the beginning of the time horizon and the beginning of time period $p\in P^s$
\item[•] $d(p)$ = time elapsed between the beginning of the time horizon and the end of time period $p\in P$
\item[•] $e_c$ = months experience in the rank for seafarer $c$
\item[•] $\tilde{e}_c$ = months experience in the company for seafarer $c$
\item[•] $\varepsilon_{c,u}=1 \Leftrightarrow e_c\geq  l_{r(c),u} > 0$    (such as $c$ is enough experienced in the rank in respect of request $u$), 0 otherwise $\qquad \forall c\in C,\, u\in U$
\item[•] $\tilde{\varepsilon}_{c,u}=1 \Leftrightarrow \tilde{e}_c\geq  \tilde{l}_{r(c),u} >0 $  (such as $c$ is enough experienced in the company in respect of request $u$), 0 otherwise $\qquad \forall c\in C,\, u\in U$
\item[•] $k(j,c)$ = cost of duty period $j\in \Omega_c$
\item[•] $l_{r,u}$ = minimum rank experience required to say that a seafarer of rank $r$ is experienced in the rank, according to rule $u$. 
\item[•] $\tilde{l}_{r,u}$ = minimum rank experience required to say that a seafarer of rank $r$ is experienced in the company, according to rule $u$ 
\item[•] $n_{s,r}$ = number of seafarers of rank $r$ required on ship $s$
\item[•] $n(u)$ = number of experienced seafarers required by rule $u$
\item[•] $r(c)$ = rank of seafarer $c$
\item[•] $\rho_{c,r}$ = 1 if seafarer $c$'s rank is $r$, 0 otherwise
\end{itemize}

\subsubsection*{Decision variables}
\hspace{6mm}• $X_j^c$ = 1 if duty period $j\in \Omega_c$ is chosen in the solution, 0 otherwise $\forall c\in C,\, j\in \Omega_c$


\subsubsection*{Constraints}

\begin{itemize}

\item[•] Selected duty periods for the same seafarer can not overlap in time
\begin{flalign}
& \hspace{40mm} \beta^c_p + \sum_{j\in \Omega_c} b_{jp}^c \cdot X^c_j \leq 1  & \forall p\in P,\, c\in C
\label{no_overlap}
\end{flalign}
\vspace{0mm}

\item[•] Every ship must have the required number of seafarers on board in every moment
\begin{flalign}
& \hspace{20mm} \sum_{c\in C} \rho_{rc} \left(\alpha_{sp}^c + \sum_{j\in \Omega_c} a_{jsp}^c \cdot   X_j^c \right) = n_{sr} & \forall s\in S,\,p\in P^s,\,r\in R
\end{flalign}

$ \alpha_{sp}^c + \sum_{j\in \Omega_c} a_{jsp}^c \cdot   X_j^c \leq 1$ for constraint \eqref{no_overlap}. In particular, it is 1 if $c$ is working on ship $s$ during period $p$ according to duty period $j$ and duty period $j$ is selected or if $c$ is working on ship $s$ during $p$ since a previous choice. By multiplying it by $\rho_{rc}$ we select only `active' seafarers whose rank is $r$.

\item[•] Enough experienced seafarers 
\begin{subequations}
\begin{flalign}
&\hspace{20mm} \sum_{c\in C} \varepsilon_{c,u} \left(\alpha_{sp}^c + \cdot \sum_{j\in \Omega_c} a_{jsp}^c \cdot   X^c_j \right) \geq n(u) & \forall s\in S,\,  p\in P^s,\, u\in U \label{experience_rank}\\
&\hspace{20mm}\sum_{c\in C} \tilde{\varepsilon}_{c,u} \cdot \left(\alpha_{sp}^c +\sum_{j\in \Omega_c} a_{jsp}^c \cdot   X^c_j \right) \geq n(u) & \forall s\in S,\,  p\in P^s,\, u\in U \label{experience_company}
\end{flalign}
\end{subequations}

These constraints require that the number of experienced in the rank \eqref{experience_rank} / company \eqref{experience_company} seafarers must satisfy the number of experienced seafarers required by rule $u$. The structure is similar to the one of the previous constraint.

\end{itemize}

\subsubsection*{Objective function}
\begin{equation*}
\sum_{c\in C} \left( \sum_{j\in \Omega_c} k(j,c)\cdot X_j^c + \gamma_c \right)
\end{equation*}






\end{document}